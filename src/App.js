import {Component} from "react";
import Movie from "./Movie-components/Movie";
import './App.css';

class App extends Component {

    constructor(props) {
        super(props);
        this.state={
            items: [],
            currentItem: {
                text: '',
                key: ''
            }
        }
        this.changeInput=this.changeInput.bind(this);
        this.addMovie=this.addMovie.bind(this);
        this.deleteItem=this.deleteItem.bind(this);
        this.changeNewInput=this.changeNewInput.bind(this);
    };

    changeInput(e) {
        this.setState({
            currentItem: {
                text: e.target.value,
                key: Date.now()
            }
        });
    };

    addMovie(e) {
        e.preventDefault();
        const newItem = this.state.currentItem;
        if (newItem.text !== "") {
            const newItems = [...this.state.items, newItem];
            this.setState({
                items: newItems,
                currentItem: {
                    text: '',
                    key: ''
                }
            })
        }
    }

    deleteItem(key) {
        const filterItems = this.state.items.filter(item => item.key !== key);
        this.setState({
            items: filterItems
        })
    }

    changeNewInput(text, key) {
        const items = this.state.items;
        items.map(item => {
            if (item.key === key) {
                item.text = text;
            }
        })
        this.setState({
            items: items
        })
    }

    render() {
        return (
            <>
                <div className="App">
                    <h2>A Movies List</h2>
                    <form className="form-main" onSubmit={this.addMovie}>
                        <input className="main_input"
                               type="text"
                               placeholder="Enter a new movie"
                               value={this.state.currentItem.text}
                               onChange={this.changeInput}/>
                        <button className="submit">Add</button>
                    </form>
                    <Movie items = {this.state.items}
                           deleteItem={this.deleteItem}
                           changeNewInput={this.changeNewInput}
                    />
                </div>
            </>
        );
    }
}

export default App;
