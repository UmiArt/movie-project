import React from 'react';
import './Movie.css';

const Movie = props => {

    const movies = props.items;
    const listMovies = movies.map(movie => {
        return <div className="list" key={movie.key}>
            <input className="input"
                   type="text" id={movie.key}
                   value={movie.text}
                   onChange={(e) => {
                       props.changeNewInput(e.target.value, movie.key)
                   }}
            />
            <button className="delete" onClick={() => props.deleteItem(movie.key)}>X</button>
        </div>
    })
    return (
        <div className="listMovies">
            <h3>To Watch List : </h3>
            <div>{listMovies}</div>
        </div>
    );
}

export default Movie;